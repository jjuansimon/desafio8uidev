import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { TiendaComponent } from './tienda/tienda.component';
import { FormularioComponent } from './formulario/formulario.component';



@NgModule({
  declarations: [
    InicioComponent,
    TiendaComponent,
    FormularioComponent
  ],
  imports: [
    CommonModule,
  ]
})

export class RoutesModule { }